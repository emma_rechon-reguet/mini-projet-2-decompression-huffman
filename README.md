# mini-projet-2-Decompression-huffman

Voici un guide permettant d'utiliser au mieux les codes

## Pour commencer

L'objectif de ce projet était de décoder une code binaire créé avec Haufman.

## Installation

Lorsqu'un interpréteur est installé (VSCode par exemple), cloner l'ensemble du code fournis dans cette branche.

## Test and Deploy

Tous les tests s'effectueront dans la partie main (Test.py).
Tester les différentes fonctionnalités en décommentant au fur et à mesure le code.
La/Les classes concernées sont notées au début.