from Arbre import Arbre
from DecodageBinaire import DecodageBinaire
from LectureFichier import LectureFichier
from LectureFichierBinaire import LectureFichierBinaire
from FichierTxt import FichierTxt
from CalculTaux import CalculTaux
from Nb_moyen_bits import Nb_moyen_bits


class Test :
    # Test de la classe LectureFichier
    """lf=LectureFichier("exemple_freq") 
    lf.getTab(lf.lines)
    lf.setAlphabet()
    lf.getTab(a)
    print(lf.setFrequence())
    lf.getTab(f)
    dico=lf.setDico()
    print(dico)"""
    
    # Test de la classe Arbre ainsi que des noeuds associés
    """abr = Arbre("exemple_freq")
    print(abr.getNoeuds())
    print(abr.toString())"""
    
    # Test de la lecture d'un fichier binaire
    #lfb=LectureFichierBinaire("exemple_comp")
    
    # Test du calcul de la profondeur
    #print(abr.getProfondeurMax())
    
    # Test associés au DécodageBinaire :
    """db=DecodageBinaire("exemple_comp")
    print(db.lectureCode(db.racine,0))"""
    
    # Test associé à l'écriture dans un fichier
    fich=FichierTxt("exemple_comp")
    fich.creerFichier("donnee")
    
    # Test associé au calcul du taux de compression
    calcul=CalculTaux("donnees/" + "decodagedonnee" + ".txt", "donnees/" + "exemple_comp" + ".bin")
    print(calcul.getTaux())
    
    #Test associé aux calculs de nombre de bits moyens
    bi=Nb_moyen_bits("exemple_comp","exemple_freq")
    #print(bi.nb_bits(bi.racine, 0))
    print(bi.calcul_moyenne())
    
    