from abc import ABC, abstractmethod

class Noeud(ABC):
    @abstractmethod
    def getLabel(): pass
    def getPoids() : pass
    def getLeftC() : pass
    def getRightC() : pass
    def egalLabel(l) : pass
    def getProfondeurMaxNoeud() : pass