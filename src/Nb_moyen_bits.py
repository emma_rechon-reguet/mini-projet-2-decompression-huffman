from Arbre import Arbre
from LectureFichier import LectureFichier
from LectureFichierBinaire import LectureFichierBinaire


class Nb_moyen_bits :
    def __init__(self, nombinaire, nomFichier):
        #on récupère les infos
        lfb=LectureFichierBinaire(nombinaire)
        lf=LectureFichier(nomFichier)
        self.alpha=lf.setAlphabet()
        self.code=lfb.getCode()
        abr=Arbre(nomFichier)
        
        noeuds=abr.getNoeuds()
        self.racine=noeuds[0]
        self.taille=noeuds[0].getPoids()   
        temp=self.code.split(" ")
        self.code=""
        for code in temp:
            self.code+=code
        self.bits=dict()
    
    def element_de(self, liste, elem):
        for element in liste :
            if(element==elem ) :
                return True
        return False
    
    def nb_bits(self, noeud, idx):
        while (len(self.bits)<self.taille and idx<len(self.code)):
            if noeud is not None :
                if noeud.getLabel() is not None:
                    print(noeud.getLabel())
                    self.code=self.code[idx:]
                    if not self.element_de(self.bits, noeud.getLabel()): 
                        self.bits[noeud.getLabel()]= idx
                    return self.nb_bits(self.racine, 0)
                if int(self.code[idx]) == 0:
                    idx += 1
                    return self.nb_bits(noeud.getLeftC(), idx)
                elif int(self.code[idx]) == 1:
                    idx += 1
                    return self.nb_bits(noeud.getRightC(), idx)
        return self.bits
    
    def calcul_moyenne(self):
        self.nb_bits(self.racine, 0)
        somme=0
        for elem in self.alpha:
            somme+=self.bits[elem]
        moy=somme/len(self.alpha)
        return moy