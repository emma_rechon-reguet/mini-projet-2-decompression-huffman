from Noeud import Noeud


class NoeudAutre(Noeud):
    def __init__(self, leftC, rightC):
        self.leftC=leftC
        self.rightC=rightC
        leftC.parent=self
        rightC.parent=self
    
    def getLabel(self):
        return None
    
    def getPoids(self) :
        return self.leftC.getPoids()+self.rightC.getPoids()
    
    def getLeftC(self):
        return self.leftC
    
    def getRightC(self):
        return self.rightC
    
    def toString (self) :
        label_str = self.getLabel() if self.getLabel() is not None else "None"
        left_child_str = self.leftC.toString() if self.leftC is not None else "None"
        right_child_str = self.rightC.toString() if self.rightC is not None else "None"
        return "Le noeud '" + label_str + "' a comme poids " + str(self.getPoids()) + " qui a comme enfants " + left_child_str + ";" + right_child_str  
    
    def egalLabel(self, l):
        return False
    
    
    def getProfondeurMaxNoeud(self) :
        if not self :
            return
        else : 
            prof_g=self.getLeftC().getProfondeurMaxNoeud()
            prof_d=self.getRightC().getProfondeurMaxNoeud()
            return max(prof_g,prof_d)+1