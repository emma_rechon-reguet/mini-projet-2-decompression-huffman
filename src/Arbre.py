from LectureFichier import LectureFichier
from NoeudAutre import NoeudAutre
from NoeudFeuille import NoeudFeuille


class Arbre():
    def __init__(self, nom):
        lf=LectureFichier(nom)
        noeudsEntrants=lf.setDico()
        self.noeudArbre=[]
        self.noeudsRestants=[]
        
        self.p_max=0
        
        for lettre, poids in noeudsEntrants.items():
            poids_entier = int(poids)
            noeud_final = NoeudFeuille(lettre, int(poids))
            self.noeudsRestants.append(noeud_final)
            self.noeudArbre.append(noeud_final)
            self.p_max += poids_entier
            
        while len(self.noeudsRestants) > 1:
            n = NoeudAutre(self.noeudsRestants[0], self.noeudsRestants[1])
            self.noeudsRestants.pop(0)
            self.noeudsRestants.pop(0)
            poids = int(n.getPoids())
            idx = 0
            while idx < len(self.noeudsRestants) and self.noeudsRestants[idx].getPoids() < poids:
                idx += 1
            self.noeudsRestants.insert(idx, n)
            self.noeudArbre.insert(idx, n)
        self.noeudArbre.append(self.noeudsRestants[0])
        self.noeudTete = self.noeudsRestants[0]
        self.noeudArbre.reverse()
        
    def getNoeuds(self):
        return self.noeudArbre

    def getNoeudTete(self):
        return self.noeudTete

    def getPoidsMax(self):
        return self.p_max
    
    def toString(self):
        return self.noeudArbre[len(self.noeudArbre)-1].toString()
    
    def getProfondeurMax(self):
        racine=self.noeudArbre[len(self.noeudArbre)-1]
        return racine.getProfondeurMaxNoeud()-1
    
    
        
            
            
    