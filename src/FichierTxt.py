import os
from DecodageBinaire import DecodageBinaire


class FichierTxt :
    
    def __init__(self, nom):
        #self.chemin=
        #nom correspond au nom du fichier pour le décodage binaire
        self.db=DecodageBinaire(nom)
        self.path="donnees\decodage"
    
    def creerFichier(self,nom):
        self.path+=nom+".txt"
        if not os.path.exists(self.path):
            with open(self.path, "a") as fichier:
                fichier.write(self.db.lectureCode(self.db.racine,0))
        else :
            os.remove(self.path)
            self.creerFichier(nom)