from Arbre import Arbre
from LectureFichier import LectureFichier
from LectureFichierBinaire import LectureFichierBinaire
from NoeudFeuille import NoeudFeuille


class DecodageBinaire:
    
    def __init__(self,nom):
        #on récupère les infos
        lfb=LectureFichierBinaire(nom)
        self.code=lfb.code_binaire
        abr=Arbre("exemple_freq")
        self.noeuds=abr.getNoeuds()
        self.racine=self.noeuds[0]
        self.taille=self.noeuds[0].getPoids()
        temp=self.code.split(" ")
        self.code=""
        for code in temp:
            self.code+=code
        self.result=""
            
    def lectureCode(self,noeud,idx):
        while (len(self.result)<self.taille and idx<len(self.code)):
            if noeud is not None :
                if noeud.getLabel() is not None:
                    self.result+=noeud.getLabel()
                    self.lectureCode(self.racine,idx)
                if int(self.code[idx]) == 0:
                    idx += 1
                    return self.lectureCode(noeud.getLeftC(), idx)
                elif int(self.code[idx]) == 1:
                    idx += 1
                    return self.lectureCode(noeud.getRightC(), idx)
        return self.result           