class LectureFichierBinaire :
    def __init__(self, nom):
        with open("donnees\\"+nom+'.bin', 'rb') as fichier:
            self.donnees = fichier.read()
            self.code_binaire = ' '.join(format(octet, '08b') for octet in self.donnees)
            
    def getCode(self):
        return self.code_binaire
        