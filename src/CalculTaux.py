import os

class CalculTaux:
    def __init__(self, txtpath, binpath):
        volbin=0
        voltxt=0
        self.taux=0
        if os.path.exists(txtpath) :
            voltxt = os.path.getsize(txtpath)
        if os.path.exists(binpath):
            volbin = os.path.getsize(binpath)
        if(volbin!=0 and voltxt!=0):
            self.taux=1-(volbin/voltxt)        
        
    def getTaux(self):
        return self.taux