from Noeud import Noeud

class NoeudFeuille(Noeud):
    def __init__(self,label,poids):
        super()
        self.label=label
        self.poids=poids
        
    def getLabel(self):
        return self.label
    
    def getPoids(self):
        return self.poids
    
    def getLeftC(self):
        return None
    
    def getRightC(self):
        return None
    
    def toString(self):
        return "Le noeud '"+ self.getLabel() + "' a comme poids "+ str(self.getPoids())
    
    def egalLabel(self, l):
        return self.label==l
    
    def getProfondeurMaxNoeud(self):
        return 1