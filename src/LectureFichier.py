class LectureFichier :

    def __init__ (self, nom):
        #initialisation des valeurs
        self.lines=[]
        self.alphabet=[]
        self.frequence=[]
        self.alphafreq=dict()
        #lecture du fichier
        nom_fichier="donnees\\"+nom+".txt"
        fichier = open("donnees\\"+nom+".txt", "r")
        lines = fichier.readlines()
        #lecture ligne par ligne
        with open(nom_fichier, "r") as fichier:
            self.lines = fichier.readlines()
        fichier.close()
    
    def getLines(self):
        #afficher ce qui est obtenu sur une ligne (pour tester)
        for line in self.lines :
            print(line)

    def setAlphabet(self):
        #défini l'alphabet
        if '7\n' in self.lines :
            self.lines.remove('7\n')
        for line in self.lines :
            element = line.split(' ')
            self.alphabet.append(element[0])
        return self.alphabet
        
    def getLengthAlpahabet(self):
        #donne la taille de l'alphabet, et donc la largeur maximale
        return self.lines[0]
    
    def getTab(self, tab):
        #affiche les éléments du tableau passer en paramètre
        for lines in tab :
            print(lines)
            
    def setFrequence(self):
        #définit toutes les fréquences données
        if '7\n' in self.lines :
            self.lines.remove('7\n')
        for line in self.lines :
            element = line.split(' ')
            self.frequence.append(element[1].strip())
        return self.frequence
    
    def setDico(self):
        #définition du dictionnaire qui permettra de créer l'arbre
        self.setFrequence()
        self.setAlphabet()
        self.alphafreq.update({self.alphabet[0]: self.frequence[0]})
        for i in range (1,len(self.frequence)) :
            self.alphafreq.update({self.alphabet[i] : self.frequence[i]})
        return self.alphafreq
    